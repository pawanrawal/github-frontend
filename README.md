# Github Frontend

The project lets you view open/closed pull requests for a repo. It also allows you to merge open
pull requests.

# Running the project

1. Run `npm install` from the root of the repo.

2. Set a couple of environment variables.

```
# To change the repo for which you want to view the PR's.
export REACT_APP_REPO=facebook/react

# So that you can merge PR's using the UI.
export REACT_APP_TOKEN=github_user_name:github_personal_access_token
```

If the token is not supplied or is invalid, merging of PR's wouldn't be allowed.

See instructions on how to create a token [here](https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line/).

3. After this, the project can be run using the following command from the root of the repo.
```
npm run start
```
The command should be run in the same shell that you ran the export commands in.


This would open a browser for you and the app would be running on port 3000.

---

The project is also available on https://gitlab.com/pawanrawal/github-frontend