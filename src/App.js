import React, { Component } from "react";
import "./App.css";

import parse from "parse-link-header";

function isEmpty(str) {
  return !str || 0 === str.length;
}

const REPO = isEmpty(process.env.REACT_APP_REPO)
  ? "facebook/react"
  : process.env.REACT_APP_REPO;

const TOKEN = process.env.REACT_APP_TOKEN || "";

const PAGE_SIZE = 25;
const BASE_URL = "https://api.github.com/repos/" + REPO + "/pulls";
const PULL_REQUESTS = BASE_URL + "?per_page=" + PAGE_SIZE + "&";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pullRequests: [],
      current: 0,
      next: 1,
      type: "closed",
      isFetching: "true",
      error: "",
      success: ""
    };
  }

  updatePage = delta => {
    this.setState({
      isFetching: true
    });
    window.scrollTo(0, 0);

    var pageToFetch = this.state.current + delta;

    var that = this;
    let headers = new Headers();

    if (TOKEN.length > 0) {
      headers.append("Authorization", "Basic " + btoa(TOKEN));
    }
    fetch(PULL_REQUESTS + "state=" + this.state.type + "&page=" + pageToFetch, {
      method: "GET",
      headers: headers
    })
      .then(function(response) {
        that.setState({
          isFetching: false
        });

        if (!response.ok) {
          throw Error(response.statusText);
        }

        that.setState({
          current: pageToFetch
        });

        var linkHeader = response.headers.get("Link");
        // When there are no more pages, Github doesn't send back a link header in response.
        if (linkHeader == null) {
          that.setState({
            next: -1
          });
          return response.json();
        }

        var parsed = parse(linkHeader);

        that.setState({
          next: parsed.next.page
        });
        return response.json();
      })
      .then(data => this.setState({ pullRequests: data }))
      .catch(error => {
        this.setState({
          error:
            "Error from Github while fetching " +
            this.state.type +
            " PR's: " +
            error.message
        });
      });
  };

  merge = e => {
    var that = this;
    var number = e.target.dataset.number;
    let headers = new Headers();
    headers.append("Authorization", "Basic " + btoa(TOKEN));
    fetch(BASE_URL + "/" + number + "/merge", {
      method: "PUT",
      headers: headers
    })
      .then(function(response) {
        if (!response.ok) {
          throw Error(response.statusText);
        }

        that.setState({
          success: "PR #" + number + " merged successfully."
        });
        // Fetch PR's again and re-render.
        that.updatePage(0);

        return null;
      })
      .catch(error => {
        this.setState({
          error: "Got error while trying to merge PR: " + error.message
        });
      });
  };

  updateType = e => {
    if (this.state.type === e.target.dataset.value) {
      return;
    }

    this.setState({
      type: e.target.dataset.value,
      next: 1,
      current: 0
    });
  };

  componentDidMount() {
    this.updatePage(1);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.type === this.state.type) {
      return;
    }

    this.updatePage(1);
  }

  render() {
    const pullRequests = this.state.pullRequests;
    const open = this.state.type === "open" ? "active" : "";
    const closed = this.state.type === "closed" ? "active" : "";
    const next = this.state.next === -1 ? "disabled" : "";
    const previous = this.state.current === 1 ? "disabled" : "";
    const isFetching = this.state.isFetching;

    // TODO - Handle zero pr's
    return (
      <div>
        <nav className="navbar navbar-toggleable-md navbar-inverse bg-primary">
          <button
            className="navbar-toggler navbar-toggler-right"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <a
            className="navbar-brand"
            href={`https://github.com/${REPO}`}
            target="_blank"
          >
            {REPO}
          </a>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className={`nav-item ${closed}`}>
                <a
                  className="nav-link"
                  onClick={e => this.updateType(e)}
                  data-value="closed"
                >
                  Closed PR's <span className="sr-only">(current)</span>
                </a>
              </li>
              <li className={`nav-item ${open}`}>
                <a
                  className="nav-link"
                  onClick={e => this.updateType(e)}
                  data-value="open"
                >
                  Open PR's
                </a>
              </li>
            </ul>
          </div>
        </nav>

        <div style={{ marginTop: "10px" }}>
          {isFetching ? <h3>Loading...</h3> : ""}
        </div>

        {this.state.success !== "" ? (
          <div
            style={{ marginTop: "10px" }}
            className="text-center alert alert-success alert-dismissible fade show"
            role="alert"
          >
            {this.state.success}
            <button
              type="button"
              className="close"
              data-dismiss="alert"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        ) : (
          ""
        )}

        {this.state.error !== "" ? (
          <div
            style={{ marginTop: "10px" }}
            className="text-center alert alert-danger alert-dismissible fade show"
            role="alert"
          >
            {this.state.error}
            <button
              type="button"
              className="close"
              data-dismiss="alert"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        ) : (
          ""
        )}

        {this.state.type === "open" &&
        pullRequests.length > 0 &&
        TOKEN.length === 0 ? (
          <div
            style={{ marginTop: "10px" }}
            className="text-center alert alert-warning alert-dismissible fade show"
            role="alert"
          >
            Token must be supplied as an environment variable to merge pull
            requests.
            <button
              type="button"
              className="close"
              data-dismiss="alert"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        ) : (
          ""
        )}

        {this.state.pullRequests.length > 0 ? (
          <div>
            <div
              className="col-md-8 offset-md-2"
              style={{ opacity: isFetching ? 0 : 1 }}
            >
              <table className="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                  </tr>
                </thead>
                <tbody>
                  {pullRequests.map((pr, idx) => (
                    <tr key={pr.id}>
                      <th scope="row">
                        {(this.state.current - 1) * PAGE_SIZE + idx + 1}.
                      </th>
                      <td>
                        <a href={pr.html_url} target="_blank">
                          {pr.title}
                        </a>
                      </td>
                      {pr.state === "open" ? (
                        <td className="text-right">
                          <button
                            type="button"
                            className="btn btn-success"
                            onClick={e => this.merge(e)}
                            data-number={pr.number}
                            disabled={TOKEN.length === 0}
                          >
                            Merge
                          </button>
                        </td>
                      ) : (
                        <td />
                      )}
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            <div
              className="row"
              style={{ justifyContent: "center", marginBottom: "20px" }}
            >
              <nav aria-label="Page navigation">
                <ul className="pagination text-center">
                  <li className="page-item">
                    <button
                      className={`btn page-link ${previous}`}
                      onClick={() => this.updatePage(-1)}
                    >
                      Previous
                    </button>
                  </li>
                  <li className="page-item">
                    <button
                      className={`btn page-link ${next}`}
                      onClick={() => this.updatePage(1)}
                    >
                      Next
                    </button>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
}

export default App;
